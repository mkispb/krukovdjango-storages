import os.path
import mimetypes

from django.core.files.base import ContentFile
from django.core.files.storage import Storage
from django.core.exceptions import ImproperlyConfigured

try:
    import azure
    import azure.storage
except ImportError:
    raise ImproperlyConfigured(
        "Could not load Azure bindings. "
        "See https://github.com/WindowsAzure/azure-sdk-for-python")

from storages.utils import setting


def clean_name(name):
    return os.path.normpath(name).replace("\\", "/")


class AzureStorage(Storage):
    account_name = setting("AZURE_ACCOUNT_NAME")
    account_key = setting("AZURE_ACCOUNT_KEY")
    azure_container = setting("AZURE_CONTAINER")
    azure_ssl = setting("AZURE_SSL")
    azure_protocol = 'https:' if azure_ssl else 'http:' if azure_ssl is not None else ''

    def __init__(self, *args, **kwargs):
        super(AzureStorage, self).__init__(*args, **kwargs)
        self._connection = None

    @property
    def connection(self):
        if self._connection is None:
            self._connection = azure.storage.BlobService(
                self.account_name, self.account_key)
        return self._connection

    @property
    def azure_bucket(self):
        return '%s//%s%s/%s' % (self.azure_protocol, self.account_name, self.connection.host_base, self.azure_container)

    def _open(self, name, mode="rb"):
        try:
            contents = self.connection.get_blob(self.azure_container, name)
        except Exception:
            raise IOError
        return ContentFile(contents)

    def exists(self, name):
        try:
            self.connection.get_blob_properties(
                self.azure_container, name)
        except azure.WindowsAzureMissingResourceError:
            return False
        else:
            return True

    def delete(self, name):
        self.connection.delete_blob(self.azure_container, name)

    def size(self, name):
        properties = self.connection.get_blob_properties(
            self.azure_container, name)
        return properties["content-length"]

    def _save(self, name, content):
        try:
            content_type = content.file.content_type
        except AttributeError:
            content_type = mimetypes.types_map.get('.' + name.split('.')[-1])
        self.connection.put_blob(self.azure_container, name,
                                 content.read(),
                                 x_ms_blob_type='BlockBlob',
                                 x_ms_blob_content_type=content_type)
        return name

    def url(self, name):
        return "%s/%s" % (self.azure_bucket, name)
